(setq current-location (file-name-directory (or load-file-name (buffer-file-name))))

(require 'auto-complete)
(require 'auto-complete-config)
(global-auto-complete-mode t)
(add-to-list 'ac-dictionary-directories (format "%sauto-complete/ac-dict" current-location))
(ac-config-default)

(add-hook 'c-mode-common-hook
          '(lambda()
             (linum-mode)))

(add-hook 'emacs-lisp-mode-hook
          '(lambda()
             (linum-mode)))

(add-hook 'LaTeX-mode-hook
          '(lambda()
             (linum-mode)))

(add-hook 'python-mode-hook
          '(lambda()
             (linum-mode)))


(setq c-default-style "stroustrup")
(provide 'init-auto-complete)