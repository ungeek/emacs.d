(require 'auto-complete-clang)
;(ac-set-trigger-key "TAB")
(define-key ac-mode-map  [(control tab)] 'auto-complete)
(defun super-ac-config ()
(setq ac-clang-flags
      (mapcar (lambda (item)(concat "-I" item))
              (split-string
               "E:/TDM-GCC-64/x86_64-w64-mingw32/include
				E:/TDM-GCC-64/lib/gcc/x86_64-w64-mingw32/4.8.1/include
				E:/lib/boost_1_56_0
				E:/TDM-GCC-64/lib/gcc/x86_64-w64-mingw32/4.8.1/include/c++/x86_64-w64-mingw32
				F:/workspace/chromium"
               )))
(setq-default ac-sources '(ac-source-filename
                            ac-source-functions
                            ac-source-yasnippet
                            ac-source-variables
                            ac-source-symbols
                            ac-source-features
                            ac-source-abbrev
                            ac-source-words-in-same-mode-buffers
                            ac-source-dictionary))
  (add-hook 'c-mode-common-hook 'ac-cc-mode-setup)
  (add-hook 'auto-complete-mode-hook 'ac-common-setup)
  (global-auto-complete-mode t)
)
(super-ac-config)
(defun super-ac-cc-mode-setup ()
  (setq ac-sources (append '(ac-source-clang ac-source-yasnippet) ac-sources)))
(add-hook 'c-mode-common-hook 'super-ac-cc-mode-setup)
(provide 'init-auto-complete-clang)
			   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
               ;; E:/lib/hpl_stl                                                ;;
			   ;; E:/TDM-GCC-64/x86_64-w64-mingw32/include                      ;;
			   ;; E:/TDM-GCC-64/lib/gcc/x86_64-w64-mingw32/4.8.1/include        ;;
			   ;; E:/TDM-GCC-64/lib/gcc/x86_64-w64-mingw32/4.8.1/include/c++    ;;
               ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
