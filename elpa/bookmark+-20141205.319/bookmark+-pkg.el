(define-package "bookmark+" "20141205.319" "Bookmark+: extensions to standard library `bookmark.el'." 'nil :url "http://www.emacswiki.org/bookmark+.el" :keywords
  '("bookmarks" "bookmark+" "placeholders" "annotations" "search" "info" "url" "w3m" "gnus"))
;; Local Variables:
;; no-byte-compile: t
;; End:
